package frc.robot.commands;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.ArcadeDrive;

public class AutoCommand extends CommandBase {

  private final ArcadeDrive driveSubsystem;
  private final Timer m_timer = new Timer();

  public AutoCommand(ArcadeDrive driveSubsystem) {
    this.driveSubsystem = driveSubsystem;
    addRequirements(driveSubsystem);
  }

  @Override
  public void initialize() {
    m_timer.reset();
    while (m_timer.get() <= 10.0) {
      driveSubsystem.drive(0.5, 0);
    }
  }

  @Override
  public void execute() {
    if (m_timer.get() > 10.0) {
      driveSubsystem.stop();
      driveSubsystem.drive(0, 0.5);
    }
  }

  @Override
  public void end(boolean interrupted) {
    driveSubsystem.stop();
  }
}
