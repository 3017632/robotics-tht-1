package frc.robot;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.RunCommand;
import frc.robot.commands.AutoCommand;
import frc.robot.subsystems.ArcadeDrive;

public class RobotContainer {

  private final ArcadeDrive driveSubsystem = new ArcadeDrive();
  private final AutoCommand autoCommand = new AutoCommand(driveSubsystem);
  private Joystick ps4 = new Joystick(0);

  public RobotContainer() {
    double forward = -ps4.getY();
    double turn = ps4.getX();
    driveSubsystem.setDefaultCommand(
      new RunCommand(() -> {
        driveSubsystem.drive(forward, turn);
      })
    );
  }

  public Command getAutonomousCommand() {
    return autoCommand;
  }
}
