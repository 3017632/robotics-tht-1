package frc.robot.subsystems;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

public class ArcadeDrive extends SubsystemBase {

  private CANSparkMax frontLeft = new CANSparkMax(1, MotorType.kBrushless);
  private CANSparkMax frontRight = new CANSparkMax(2, MotorType.kBrushless);
  private CANSparkMax backLeft = new CANSparkMax(3, MotorType.kBrushless);
  private CANSparkMax backRight = new CANSparkMax(4, MotorType.kBrushless);
  private DifferentialDrive differentialDrive = new DifferentialDrive(
    frontLeft,
    frontRight
  );

  public ArcadeDrive() {
    frontLeft.setInverted(false);
    backLeft.setInverted(false);
    frontRight.setInverted(false);
    backRight.setInverted(false);
  }

  public void stop() {
    differentialDrive.stopMotor();
  }

  public void drive(double forward, double turn) {
    differentialDrive.arcadeDrive(forward, turn);
  }
}
